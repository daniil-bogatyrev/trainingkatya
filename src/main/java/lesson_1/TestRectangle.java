package lesson_1;

public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(8, 8);
        rectangle.area();
        rectangle.perimetr();

        rectangle.moveLeft(1);
        rectangle.moveDown(4);
        rectangle.moveRight(3);
        rectangle.moveUp(3);
        rectangle.getCoordinates(rectangle.x, rectangle.y);
        System.out.println();

        System.out.println("Color rectangle is = " + rectangle.color);

    }
}