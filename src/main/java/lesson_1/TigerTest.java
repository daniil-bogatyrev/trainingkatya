package lesson_1;

public class TigerTest {
    public static void main(String[] args){
        Tiger tiger = new Tiger("Kili");
        System.out.println("Tiger name is " + tiger.name);
        tiger.eat();
        tiger.play("forest");
        tiger.moveLeft(6);
        tiger.moveRight(3);
        tiger.moveDown(2);
        tiger.moveUp(5);
        tiger.getCoordinates(tiger.x, tiger.y);

    }
}