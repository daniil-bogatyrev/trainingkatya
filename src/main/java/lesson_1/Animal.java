package lesson_1;

public abstract class Animal implements Movable {
    int x=0;
    int y=0;
    String name;

    public Animal(String name){
        this.name = name;
    }
    abstract void eat();
    abstract void play(String place);

    @Override
    public void moveLeft(int step) {
        this.x = x-step;

    }

    @Override
    public void moveRight(int step) {
        this.x = x+step;
    }

    @Override
    public void moveDown(int step) {
        this.y = y-step;
    }

    @Override
    public void moveUp(int step) {
        this.y = y+step;

    }
    public void getCoordinates(int x, int y) {
        System.out.println("Координаты равны " + "x=" + x +"; y="+ y);

    }
}
