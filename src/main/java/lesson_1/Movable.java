package lesson_1;

public interface Movable {

    void moveLeft(int step);
    void moveRight(int step);
    void moveDown(int step);
    void moveUp(int step);

}
