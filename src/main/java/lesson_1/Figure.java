package lesson_1;

public abstract class Figure implements Movable{

    public int x = 0;
    public int y = 0;
    public String color; //цвет

    abstract void area();
    abstract void lenght();

    public Figure(String color){
        this.color = color;
    }

    @Override
    public void moveLeft(int step) {
        this.x = this.x - step;
        System.out.println("Сдвиг влево составляет: " + step);
    }
    @Override
    public void moveRight(int step) {
        this.x = this.x + step;
        System.out.println("Сдвиг вправо составляет: " + step);
    }

    @Override
    public void moveDown(int step) {
        this.y = this.y - step;
        System.out.println("Сдвиг вниз составляет: " + step);
    }
    @Override
    public void moveUp(int step) {
        this.y = this.y + step;
        System.out.println("Сдвиг вверх составляет: " + step);
    }

    public void getCoordinates(int x, int y) {
        System.out.println("Координаты равны " + "x=" + x +"; y="+ y);
    }
}