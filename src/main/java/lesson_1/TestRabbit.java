package lesson_1;

public class TestRabbit {
    public static void main(String[] args){
        Rabbit rabbit = new Rabbit("Limonchik");
        System.out.println("Rabbit name is " + rabbit.name);
        rabbit.eat();
        rabbit.play("the lawn");
        rabbit.moveLeft(2);
        rabbit.moveRight(2);
        rabbit.moveDown(5);
        rabbit.moveUp(3);
        rabbit.getCoordinates(rabbit.x, rabbit.y);
    }
}
