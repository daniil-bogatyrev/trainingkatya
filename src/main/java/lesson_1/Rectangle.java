package lesson_1;

public class Rectangle extends Figure {
    int width; // ширина
    int height; // высота

    public Rectangle(int width, int height) {
        super("Yellow");
        this.width = width;
        this.height = height;
        if (height==width){
            System.out.println("Является квадратом");
        }
        else {
            System.out.println("Квадратом не является");
        }

    }

    @Override
    public void area() {
        int area = width * height;
        System.out.println("Area of rectangle is : " + area);
    }

    @Override
    public void lenght() {

    }

    public void perimetr() {
        int perimetr = 2 * (width + height);
        System.out.println("Perimetr of rectangle is : " + perimetr);
    }


}

