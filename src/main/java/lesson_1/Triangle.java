package lesson_1;

public class Triangle extends Figure {

    public double a = 2.5;
    public double h = 5;
    public double r = 8.2;


    public Triangle(double a, double h){
        super("Red");
        this.a = a;
        this.h = h;
    }

    @Override
    public void area() {
        double area = 0.5 * this.a * this.h;
        System.out.println("Площадь треугольника равна = " + area);
    }

    @Override
    public void lenght() {
        double lenght = r*Math.sqrt(3);
        System.out.println("Сторона треугольника равностороннего через радиус описанной окружности равна " + lenght);


    }

}
