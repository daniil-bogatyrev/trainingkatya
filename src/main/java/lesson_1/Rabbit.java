package lesson_1;

public class Rabbit extends Animal {
    public Rabbit(String name) {
        super("Limonchik");
    }

    @Override
    void eat() {
        System.out.println("Rabbit eats food and grass!");
    }

    @Override
    void play(String place) {
        System.out.println("Rabbit playing on the " + place);
    }
}