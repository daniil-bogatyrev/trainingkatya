package lesson_1;

import java.util.ArrayList;
import java.util.List;

public class Spisok_1 {
    public static void main (String[] args) {
        List<String> city = new ArrayList<String>();
        city.add("Saratov");
        city.add("Penza");
        city.add("Kursk");
        city.add("Rostov");
        city.add("Lipeck");
        for (int i=0; i<5; i++) { // пункт 3б. Тут лучше использовать i<city.size()
            System.out.println(city.get(i)); //
        }
        System.out.println();
        city.set(0, "Moscow");
        System.out.println(city);
        System.out.println();
        city.remove(2); // пункт 3г. Тут нужно было удалить последний элемент + попробуй сделать это не используя конкретный индекс)
        System.out.println(city);
        System.out.println();
        System.out.println(city.size());
        System.out.println();
        city.remove(3);
                                // В строке 26 и 28 (п.3е) тоже лучше удалять через последний элемент
        city.remove(2);
        for (int i=0; i< city.size(); i++) {
            System.out.println(city);
        }
    }
}

