package lesson_1;

public class Tiger extends Animal {
    public Tiger(String name) {
        super("Kili");
    }

    @Override
    void eat() {
        System.out.println("Tiger eats meat!");
    }

    @Override
    void play(String place) {
        System.out.println("Tiger playing at the " + place);
    }

}
